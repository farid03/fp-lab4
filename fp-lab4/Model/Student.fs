module fp_lab4.Model

type Student =
    { id: int
      fio: string
      gender: string
      phone: string
      work: string
      education: string
      photo: string }
